class RenameColumnToUser < ActiveRecord::Migration
  def change
  	rename_column :users, :surname, :middle_name
  	rename_column :users, :phone_no, :phone
  end
end
